#!/bin/bash

parse_openvz_conf() {
	unset HOSTNAME
	unset SEARCHDOMAIN
	unset IP_ADDRESS

	source $i

	[ "$HOSTNAME" == "" ] && return
	[ "$SEARCHDOMAIN" == "" ] && return
	[ "$IP_ADDRESS" == "" ] && return
	DNS=$HOSTNAME

	[ "$DNS" == "${DNS/\./}" ] && DNS="$DNS.$SEARCHDOMAIN"
	for j in $IP_ADDRESS
	do
		echo pass dns-static name=$DNS
		echo pass dns-static address=$j
		echo add dns-static address=$j name=$DNS disabled=false ttl=00:30:00
		return
	done
}

create_confs() {
	echo section dns-static /ip/dns/static addset name
	echo pass dns-static ttl=00:30:00
	for i in /etc/pve/nodes/*/openvz/*.conf
	do
		parse_openvz_conf $i
	done
}

if [ ! -f "$1" ]
then
	echo "usage: $0 <config-file>"
	exit 1
fi

(
read HOST
read LOGIN
read PASSWORD

if [ "$HOST" == "" ] || [ "$LOGIN" == "" ]
then
	echo "ERROR: no host, no login or no password defined"
	echo ""
	echo "create $1 and fill connection details:"
	echo "<host>"
	echo "<login>"
	echo "<password>"
	echo ""
	exit 2
fi

create_confs | php `dirname $0`/../routeros.exec.php -h $HOST -l $LOGIN -p $PASSWORD -f php://stdin
) < $1
